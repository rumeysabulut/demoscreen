import { StyleSheet } from 'react-native';



const styles = StyleSheet.create({
    menu: {
        /*Use flex in a component's style to have the component expand and shrink dynamically based on available space.
          Normally you will use flex: 1, which tells a component to fill all available space,
          shared evenly amongst other components with the same parent.*/
        // Menus have the same parent
        flex: 1,
        marginTop: 16,
        marginLeft: 16,
        marginRight: 16,
        marginBottom: 0,
    },
    dropdown_header: {
        height: 54,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: 'rgb(25, 166, 145)',
    },
    dropdown_headertext: {
        padding: 16,
        fontSize: 18,
        color: 'rgb(25, 166, 145)',
        textAlign: 'center',
        textAlignVertical: 'center',
    },
    dropdown_dropdownStyle: {
        height: 'auto',
        borderColor: 'rgb(25, 166, 145)',
        borderWidth: 1,  //border kalınlığı
        borderRadius: 5,
    },
    dropdown_dropdownTextStyle: {  //açılan menüdeki yazılar
        fontSize: 15,
        backgroundColor: '#FFFFFF',
        color: 'green'
    },
});

export default styles;
import {StyleSheet} from "react-native";


const styles = StyleSheet.create({
    lineStyle: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: '#E8E8E8',
    },
    lastLine: {
        flexDirection: 'row',
    },

});

export default styles;
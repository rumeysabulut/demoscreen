import React from "react";
import {View, Text} from "react-native";
import styles from './styles';
const HeaderFinance = (props) => {
    return(
        <View style={styles.headerStyling}>
            <Text style={styles.headerText}> {props.header}</Text>
        </View>
    );

};

export default HeaderFinance;
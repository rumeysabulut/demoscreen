import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({

    cardStyle: {
        borderColor: 'rgba(0, 0, 0, 0.13)',
        borderWidth: 1,
        borderRadius: 5,
    },
    headerRegion: {
        backgroundColor: 'rgb(247, 247, 247)',
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        //buraya vereceğin margin, financeHeader'ın kartın borderlarına olan mesafesini ayarlıyor.

    },

    cardPadding: {
        paddingLeft: 10,  //kart içindeki çizgilerin ve yazıların border'a uzaklığı
        paddingRight: 10, //çizgilerin ve kart içindeki yazıların border'a uzaklığı
    },
    edges: {
        backgroundColor: 'rgb(255, 255, 255)',
        marginTop: 0, //her bir kartın üstündeki boşluk. alt alta gelen kartlar arasındaki boşluk
        marginRight: 16,
        marginLeft: 16,
        marginBottom: 16,
        shadowColor: 'rgba(0, 0, 0, 0.13)',  //4th term is for opacity
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        elevation: 5,
        borderColor: 'rgba(0, 0, 0, 0.06)',  //i gave the opacity
        borderWidth: 1,
        borderRadius: 5,

    },
    footerStyle: {
        flexDirection: 'row',
        backgroundColor: 'rgb(232, 246, 244)',
        borderTopWidth: 1,
        borderTopColor: 'rgb(232, 246, 244)',
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        justifyContent: 'space-evenly',

    },
    bordered: {

        padding: 10,
        borderRightWidth: 1,
        borderRightColor: '#C9C9C9'
    },
    nonbordered: {
        padding: 10
    },

});

export default styles;